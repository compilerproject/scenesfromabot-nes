.segment "ZEROPAGE"
tempBinary:    .res 2
decimalResult: .res 5

.segment "CODE"

binary_to_decimal: ; http://nesdev.parodius.com/bbs/viewtopic.php?p=10824&sid=55359b42282d1e02b91bebcf1caf56ef#10824
    lda #$00
    sta decimalResult+0
    sta decimalResult+1
    sta decimalResult+2
    sta decimalResult+3
    sta decimalResult+4
    ldx #$10 
: 
    asl tempBinary+0 
    rol tempBinary+1
    ldy decimalResult+0
    lda BinTable, y 
    rol a
    sta decimalResult+0
    ldy decimalResult+1
    lda BinTable, y 
    rol a
    sta decimalResult+1
    ldy decimalResult+2
    lda BinTable, y 
    rol a
    sta decimalResult+2
    ldy decimalResult+3
    lda BinTable, y 
    rol a
    sta decimalResult+3
    rol decimalResult+4
    dex 
    bne :-
    rts

.segment "RODATA"
BinTable:
    .byte $00, $01, $02, $03, $04, $80, $81, $82, $83, $84

.segment "CODE"

; https://gist.github.com/hausdorff/5993556
.macro mod operand_l, operand_r
    lda operand_r
    beq :++
    lda operand_l
    sec
:
    sbc operand_r
    bcs :-
    adc operand_r
:
.endmacro

.macro division operand_l, operand_r
    lda operand_r
    beq :++
    lda #0
    sec
:
    inx
    sbc operand_l
    bcs :-
    txa
:
.endmacro