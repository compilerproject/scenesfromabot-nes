.segment "ZEROPAGE"

text_address:  .res 2
text_offset:   .res 1
text_x_start:  .res 1
text_x:        .res 1
text_y:        .res 1
text_delay:    .res 1

.segment "CODE"

.macro draw_text address, x_start, y_start, frame_delay
	lda #<address
	sta text_address
	lda #>address
	sta text_address+1

	lda x_start
	sta text_x
    sta text_x_start
	lda y_start
	sta text_y

    lda frame_delay
    sta text_delay

    lda #0
    sta text_offset

	jsr _draw_text
.endmacro

_draw_text:
    jsr _strlen
    cmp #100
    bcc :+
        dec text_y
    :

	ldx #0
	ldy #0
	@loop:
	    jsr ppu_update

        mod nmi_count, text_delay
        bne @loop

        jsr next_nmt

		lda (text_address), y

        cmp #$00
        bne :+
            lda #0
            sta scroll_nmt ; resetting to first nametable
            rts ; null terminator character
        :
        cmp #$0D
        bne :+
            iny ; \r - we'll just skip past this
            inc text_offset
            jmp @loop
        :
        cmp #$0A
        bne :+
            jsr _newline ; \n
            jmp @loop
        :
        cmp #$5C
        bne :++
            jsr _handle_control_char ; control character; we'll handle the next character manually

            cmp #$00
            beq :+ ; checking to make sure we're not skipping the end of the string
                jmp @loop
            :
            rts
        :

        sec
        sbc #$20 ; converting from ASCII to CHR index
        @skip_convert:
		
		ldx text_x
        ldy text_y

		jsr ppu_update_tile

		inc text_x ; advancing to next tile
        
        lda text_x
        cmp #30 ; end of the NTSC safe area
        bcc :+
            lda text_x_start ; wrapping to next line
            sta text_x

            inc text_y
        :
		
        inc text_offset
		ldy text_offset

		jmp @loop
    rts

_handle_control_char:
    iny
    lda (text_address), y

    jsr _newline

    inc text_offset ; skip past the control character & retun

    rts

_newline:
    lda text_x_start ; newline (\n)
    sta text_x
    inc text_y

    iny
    inc text_offset

    rts

_strlen:
    ldy #0

    :
		lda (text_address), y
        iny

        cmp #0
        bne :-
    
    dey
    tya

    rts