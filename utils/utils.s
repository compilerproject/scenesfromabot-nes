.include "math.s"
.include "text.s"
.include "prng.s"
.include "graphics.s"
.include "famitone2.s"

.segment "CODE"

.macro get_addr_hi_lo in_addr, out_addr
    lda #<in_addr
    sta out_addr
    lda #>in_addr
    sta out_addr+1
.endmacro