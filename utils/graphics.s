.segment "ZEROPAGE"
nmi_lock:       .res 1 ; prevents NMI re-entry
nmi_ready:      .res 1 ; set to 1 to push a PPU frame update, 2 to turn rendering off next NMI
nmt_update_len: .res 1 ; number of bytes in nmt_update buffer
tile_data_addr: .res 2 ; starting point for tile data (for indirect addressing)

scroll_x:       .res 1 ; x scroll position
scroll_y:       .res 1 ; y scroll position
scroll_nmt:     .res 1 ; nametable select (0-3 = $2000,$2400,$2800,$2C00)

tile_flags:     .res 1 ; ---- -XXX
					   ;       |||
					   ;       ||+--- Are we reading the upper half of the 32x metatile?
					   ;       |+---- Are we reading the upper half of the 16x metatile?
					   ;       +----- Are we reading the right half of the 16x metatile?
tile_index:     .res 1 ; current tile index; used when rendering to nametable

.segment "CODE"

; ppu_update: waits until next NMI, turns rendering on (if not already), uploads OAM, palette, and nametable update to PPU
ppu_update:
	lda #1
	sta nmi_ready
	:
		lda nmi_ready
		bne :-
	rts

; ppu_skip: waits until next NMI, does not update PPU
ppu_skip:
	lda nmi_count
	:
		cmp nmi_count
		beq :-
	rts

; ppu_off: waits until next NMI, turns rendering off (now safe to write PPU directly via $2007)
ppu_off:
	lda #2
	sta nmi_ready
	:
		lda nmi_ready
		bne :-
	rts

; ppu_address_tile: use with rendering off, sets memory address to tile at X/Y, ready for a $2007 write
;   Y =  0- 31 nametable $2000
;   Y = 32- 63 nametable $2400
;   Y = 64- 95 nametable $2800
;   Y = 96-127 nametable $2C00
ppu_address_tile:
	lda $2002 ; reset latch
	tya
	lsr
	lsr
	lsr
	ora #$20 ; high bits of Y + $20
	sta $2006
	tya
	asl
	asl
	asl
	asl
	asl
	sta temp
	txa
	ora temp
	sta $2006 ; low bits of Y + X
	rts

; ppu_update_tile: can be used with rendering on, sets the tile at X/Y to tile A next time you call ppu_update
ppu_update_tile:
	pha ; temporarily store A on stack
	txa
	pha ; temporarily store X on stack
	ldx nmt_update_len
	tya
	lsr
	lsr
	lsr
	ora #$20 ; high bits of Y + $20
	sta nmt_update, X
	inx
	tya
	asl
	asl
	asl
	asl
	asl
	sta temp
	pla ; recover X value (but put in A)
	ora temp
	sta nmt_update, X
	inx
	pla ; recover A value (tile)
	sta nmt_update, X
	inx
	stx nmt_update_len
	rts

; ppu_update_byte: like ppu_update_tile, but X/Y makes the high/low bytes of the PPU address to write
;    this may be useful for updating attribute tiles
ppu_update_byte:
	pha ; temporarily store A on stack
	tya
	pha ; temporarily store Y on stack
	ldy nmt_update_len
	txa
	sta nmt_update, Y
	iny
	pla ; recover Y value (but put in Y)
	sta nmt_update, Y
	iny
	pla ; recover A value (byte)
	sta nmt_update, Y
	iny
	sty nmt_update_len
	rts

draw_to_nametable:
	; get nametable addresses
	lda scroll_nmt
	cmp #1
	beq :+
		lda #$C0 ; nametable 0 attribute address
		pha
		lda #$23
		pha
		lda #$00 ; nametable 0 tile address
		pha
		lda #$20
		pha
		jmp :++
	:	
		lda #$C0 ; nametable 1 attribute address
		pha
		lda #$27
		pha
		lda #$00 ; nametable 1 tile address
		pha
		lda #$24
		pha
	:

	; setting PPU address
	pla
	sta $2006
	pla
	sta $2006

	ldx #0
	ldy #0

	lda #0
	sta tile_index
	sta tile_flags

	; drawing tiles
	:
		lda #%00000011
		sta tile_flags
		jsr _read_metatiles_32

		lda #%00000010
		sta tile_flags
		jsr _read_metatiles_32

		lda #%00000001
		sta tile_flags
		jsr _read_metatiles_32

		lda #%00000000
		sta tile_flags
		jsr _read_metatiles_32

		lda tile_index
		clc
		adc #8
		sta tile_index
	iny
	cpy #8
	bcc :-

	; writing attributes
	; setting PPU address
	pla
	sta $2006
	pla
	sta $2006

	lda #<metatiles4
	sta tile_data_addr
	lda #>metatiles4

	clc
	adc #4
	
	sta tile_data_addr+1

	lda #0
	ldx #0
	:
		lda scroll_nmt
		cmp #1
		beq :+
			lda s0_part0, x ; left nametable attributes
			jmp :++
		:	
			lda s0_part1, x ; right nametable attributes
		:
		tay

		lda (tile_data_addr), y
		sta $2007
		inx
		cpx #$40
		bne :---
	
	jsr next_nmt
			
	rts

_read_metatiles_32:
	tya
	pha

	ldx #0
	@loop:
		lda tile_flags
		and #%11111011
		sta tile_flags
		jsr _read_metatiles_16

		lda tile_flags
		ora #%00000100
		sta tile_flags
		jsr _read_metatiles_16

		inc tile_index
		inx
		cpx #8
		bne @loop
	
	pla
	tay
	
	lda tile_index
	sec
	sbc #8
	sta tile_index

	rts

_read_metatiles_16:
	ldy tile_index
	lda scroll_nmt
		cmp #1
		beq :+
			lda s0_part0, y ; left nametable tiles
			jmp :++
		:	
			lda s0_part1, y ; right nametable tiles
		:
	tay

	lda #<metatiles4
	sta tile_data_addr
	lda #>metatiles4
	sta tile_data_addr+1

	lda tile_flags
	and #%00000100
	beq :+
		inc tile_data_addr+1
	:

	lda tile_flags
	and #%00000010
	bne :+
		inc tile_data_addr+1
		inc tile_data_addr+1
	:

	lda (tile_data_addr), y
	tay

	lda #<metatiles2
	sta tile_data_addr
	lda #>metatiles2
	sta tile_data_addr+1

	lda tile_flags
	and #%00000001
	bne :+
		inc tile_data_addr+1
		inc tile_data_addr+1
	:

	.repeat 2
	lda (tile_data_addr), y
	sta $2007
	inc tile_data_addr+1
	.endrepeat

	rts

next_nmt:
	; move to next nametable
	inc scroll_nmt
	lda scroll_nmt
	cmp #2
	bne :+
		lda #0
		sta scroll_nmt
	:

	rts