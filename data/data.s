.segment "RODATA"

palette_data:
    .byte $11,$08,$2D,$37,  $11,$30,$2D,$37,  $11,$16,$2D,$37,  $11,$08,$30,$27   ;;background palette
    .byte $11,$0F,$17,$17,  $11,$02,$38,$3C,  $11,$1C,$15,$14,  $11,$16,$3B,$29   ;;sprite palette
        
sprite_data:
    ;     vert tile attr horiz
    .byte $80, $32, $00, $80   ;sprite 0
    .byte $80, $33, $00, $88   ;sprite 1
    .byte $88, $34, $00, $80   ;sprite 2
    .byte $88, $35, $00, $88   ;sprite 3