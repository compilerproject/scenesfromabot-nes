#!/bin/bash

cd ~/scenesfromabot/scenesfromabot-nes/

mkdir -p bin
rm bin/*

# Getting latest prompt, formatting for the ROM and saving to prompt.txt
truncate -s 0 prompt.txt
suggestion=`cat ../data/sfah/suggestionQueue.json | jq -r '.[0]'`
echo ${suggestion^^} | fold -s -w28 | while read line; do echo $line >> prompt.txt; done

# Compiling into whoseline.o
echo "Compiling..."
ca65 whoseline.s -g -o bin/whoseline.o

if [[ $? == 0 ]]; then
	# Linking ROM into whoseline.nes
	echo "Linking..."
	ld65 -o bin/whoseline.nes -C whoseline.cfg bin/whoseline.o -m bin/whoseline.map.txt -Ln bin/whoseline.labels.txt --dbgfile bin/whoseline.dbg
	if [[ $? == 0 ]]; then
		echo "Success!"
		
		# Opening Retroarch to record ~25 seconds of video
		video_out="whoseline.mkv"
		
		if [ -f $video_out ]; then
			rm $video_out
		fi
		export DISPLAY=:10.0
		retroarch -L ~/snap/retroarch/423/.config/retroarch/cores/mesen_libretro.so bin/whoseline.nes -r $video_out --max-frames=1500
		if [[ $? != 0 ]]; then
			echo "10.0 didn't work, so we'll try 11.0."
			export DISPLAY=:11.0
			retroarch -L ~/snap/retroarch/423/.config/retroarch/cores/mesen_libretro.so bin/whoseline.nes -r $video_out --max-frames=1500
		fi
	else
		echo "Failure."
	fi
else
	echo "Failure."
fi

# Extracting last frame for thumbnail
#   The NES ROM takes a few frames before it begins rendering properly, so we'll insert the finished image as the starting frame.
#   That way, the video's thumbnail looks more interesting on Twitter.
ffmpeg -i whoseline.mkv -update 1 -q:v 1 last.jpg

# Adding thumbnail to start of video, for 0.1 seconds
ffmpeg -loop 1 -framerate 60 -t 0.1 -i last.jpg -t 0.1 -f lavfi -i aevalsrc=0 -i whoseline.mkv -filter_complex '[0:0] [1:0] [2:0] [2:1] concat=n=2:v=1:a=1' merged.mkv

# Converting recording for Twitter
ffmpeg -i merged.mkv -vcodec libx264 -vf "pad=ceil(iw/2)*2:ceil(ih/2)*2" -pix_fmt yuv420p -strict experimental -r 30 -t 2:20 -acodec aac -vb 1024k -minrate 1024k -maxrate 1024k -bufsize 1024k -ar 44100 -ac 2 -y whoseline.mp4

# Cleaning up intermediate files
rm whoseline.mkv
rm last.jpg
rm merged.mkv
